(function (app) {
    'use strict';

    angular
        .module('app')
        .directive('sideMenu', SideMenuDrv);

    SideMenuDrv.$inject = [];

    function SideMenuDrv() {
        var directive = {
            scope: {
                visible: '='
            },
            transclude: true,
            templateUrl: 'directives/side-menu/side-menu.html'
        };

        return directive;
    }

}());
