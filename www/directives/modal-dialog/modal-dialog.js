(function (app) {
    'use strict';

    angular
        .module('app')
        .directive('modalDialog', ModalDialogDrv);

    ModalDialogDrv.$inject = [];

    function ModalDialogDrv() {
        var directive = {
            scope: {
                id: "@",
                button: "@",
                title: "@",
                isOpen: "="
            },
            link: linkFunc,
            transclude: true,
            templateUrl: 'directives/modal-dialog/modal-dialog.html'
        };

        return directive;

        function linkFunc (scope, element, attrs) {
            scope.close = close;
            scope.apply = apply;


            function apply () {
                scope.$emit("apply", {id: scope.id});
                close();
            }

            function close () {
                scope.isOpen = false;
            }
        }
    }

}());
