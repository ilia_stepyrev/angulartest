(function (app) {
    'use strict';

    angular
        .module('app')
        .controller('MainCtrl', MainCtrl);

    MainCtrl.$inject = ['$scope', '$location'];

    function MainCtrl($scope, $location) {
        $scope.showMenu = showMenu;
        $scope.hideMenu = hideMenu;
        $scope.goTo = goTo;

        init();

        function init () {
            hideMenu();
        }

        function showMenu () {
            $scope.visibleMenu = true;
        }

        function hideMenu () {
            $scope.visibleMenu = false;
        }

        function goTo(path) {
            hideMenu();
            $location.path(path);
        }
    }

}());
