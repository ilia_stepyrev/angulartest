(function (app) {
    'use strict';

    angular
        .module('app')
        .controller('AboutCtrl', AboutCtrl);

    AboutCtrl.$inject = [];

    function AboutCtrl() {

        return this;
    }

}());
